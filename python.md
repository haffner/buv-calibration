Python resource list
====================

HOWTOs to Textbooks
-------------------

[Python for Astronomers: An Introduction to Scientific Computing](https://prappleizer.github.io/)


Data Interface Layers
---------------------
[Xarray][https://xarray.dev]
[CF Xarray][https://cf-xarray.readthedocs.io/]
[netcdf4-python][https://unidata.github.io/netcdf4-python]

[HyperSpy][https://hyperspy.org]
[Satpy][https://satpy.readthedocs.io]

Analysis Ecosystems
-------------------
[Astropy][https://www.astropy.org/]
[PANGEO][https://pangeo.io/]
[UV-CDAT][https://uvcdat.llnl.gov/]

Mapping
-------
[PyGMT][https://www.pygmt.org/]


Data pipelines
--------------
[Snakemake](https://snakemake.github.io/)

Math
----

[eofs](https://ajdawson.github.io/eofs)


Special tools
-------------

[WOUDC extCSV](https://github.com/woudc/woudc-extcsv)
